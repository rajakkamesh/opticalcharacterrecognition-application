package com.image;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;

public class Driver {

    /**
     * @param args
     */
    public static void main(String[] args) throws IOException {

        String[] inputText = ImageCracker.crackImage("C:\\TEST_IMAGE\\one.txt").replaceAll("(\\r|\\n)", "").split(" ");

//read text file and convert into string array starts
        File file = new File("C:\\TEST_IMAGE\\one.txt");
        String fileContent = FileUtils.readFileToString(file);        
        String[] outputText = fileContent.replaceAll("(\\r|\\n)", "").split(" ");
//read text file and convert into string array ends

//        System.out.println("IMAGE_TEXT_SIZE: "+inputText.length);
//        System.out.println("TEXT_FILE_SIZE: "+outputText.length);

        int mismatchCount = 0;
         Pattern pattern = Pattern.compile("[a-zA-Z0-9]");
 
    
      Matcher matcher = null ;
 
     char[] chara;
     boolean flag = true;
        for(int i = 0; i < outputText.length ; i++){
            matcher = pattern.matcher(inputText[i]);
            
            chara= inputText[i].toCharArray();
            for(char c : chara){
                if(Character.isLetterOrDigit(c) ){
                   flag= true;
                }else if(c==';' || c==':' || c=='\'' || c=='.' || c==','){
                    flag= true;
                }else if(Character.isSurrogate(c)){
                    flag = true;
                }else{
                    flag= false;
                    break;
                }
            }
            if(flag){
                System.out.println(inputText[i]);
            }else{
                System.err.println(inputText[i]);
            }
//            if(inputText[i].contains("[a-zA-Z0-9]*")){
//                System.err.println(inputText[i]);
//            }else{
//                System.out.println(inputText[i]);
//            }
//            if (inputText[i].trim().equalsIgnoreCase(outputText[i].trim())) {                
//                System.out.println(inputText[i]); 
//            }
//            else{
//                System.err.println(inputText[i]);
//                inputText[i] = inputText[i]+"~";
//                mismatchCount++;
//            }
        }
        
//        for (int j = 0; j < inputText.length; j++) {
//            System.out.println(inputText[j]);
//        }
                
        
        System.out.println("MISMATCH_COUNT: "+mismatchCount);

    }

}
